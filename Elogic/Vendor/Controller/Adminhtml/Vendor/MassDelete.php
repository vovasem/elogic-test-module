<?php

namespace Elogic\Vendor\Controller\Adminhtml\Vendor;

use Elogic\Vendor\Controller\Adminhtml\Vendor;
use Elogic\Vendor\Controller\Adminhtml\VendorAbstract;

class MassDelete extends VendorAbstract
{
    /**
     * @return void
     */
    public function execute()
    {

        // Get IDs of the selected vendors
        $vendorsIds = $this->getRequest()->getParam('selected');

        foreach ($vendorsIds as $vendorId) {
            try {
                /** @var $vendorModel \Elogic\Vendor\Model\Vendor */
                $vendorModel = $this->_vendorFactory->create();
                $vendorModel->load($vendorId)->delete();
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }
        }

        if (count($vendorsIds)) {
            $this->messageManager->addSuccess(
                __('A total of %1 record(s) were deleted.', count($vendorsIds))
            );
        }

        $this->_redirect('*/*/index');
    }
}
