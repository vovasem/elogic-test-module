<?php

namespace Elogic\Vendor\Controller\Adminhtml\Vendor;

use Elogic\Vendor\Controller\Adminhtml\VendorAbstract;

class Edit extends VendorAbstract
{
    public function execute()
    {
        $vendorId = $this->getRequest()->getParam('id');
        $model = $this->_vendorFactory->create();

        if ($vendorId) {
            $model->load($vendorId);
            if (!$model->getId()) {
                $this->messageManager->addError(__('This vendor no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = $this->_objectManager->get('Magento\Backend\Model\Session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        $this->_coreRegistry->register('elogic_vendor_vendor', $model);

        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->_resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__('Entity Vendor'));

        return $resultPage;
    }
}
