<?php

namespace Elogic\Vendor\Controller\Adminhtml\Vendor;

use Elogic\Vendor\Controller\Adminhtml\VendorAbstract;

class NewAction extends VendorAbstract
{
    protected $resultPageFactory = false;

    public function execute()
    {
        $this->_forward('edit');
    }

}
