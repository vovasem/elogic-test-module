<?php

namespace Elogic\Vendor\Controller\Adminhtml\Vendor;

use Elogic\Vendor\Controller\Adminhtml\VendorAbstract;

class Save extends VendorAbstract
{
    public function execute()
    {
        $isPost = $this->getRequest()->getPost();

        if ($isPost) {
            $vendorModel = $this->_vendorFactory->create();
            $vendorId = $this->getRequest()->getParam('vendor_id');

            if ($vendorId) {
                $vendorModel->load($vendorId);
            }
            $formData = $this->getRequest()->getParam('vendor');

            $vendorModel->setData($formData);

            try {
                $vendorModel->save();

                $this->messageManager->addSuccess(__('The vendor has been saved.'));

                if ($this->getRequest()->getParam('back')) {
                    $this->_redirect('*/*/edit', ['id' => $vendorModel->getId(), '_current' => true]);
                    return;
                }

                $this->_redirect('*/*/');
                return;
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }

            $this->_getSession()->setFormData($formData);
            $this->_redirect('*/*/edit', ['id' => $vendorId]);
        }
    }

}
