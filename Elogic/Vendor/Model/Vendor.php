<?php
namespace Elogic\Vendor\Model;

class Vendor extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'elogic_vendor_items';

    protected $_cacheTag = 'elogic_vendor_items';

    protected $_eventPrefix = 'elogic_vendor_items';

    protected function _construct()
    {
        $this->_init('Elogic\Vendor\Model\ResourceModel\Vendor');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
