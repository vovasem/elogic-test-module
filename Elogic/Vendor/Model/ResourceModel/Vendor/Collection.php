<?php
namespace Elogic\Vendor\Model\ResourceModel\Vendor;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'vendor_id';
    protected $_eventPrefix = 'elogic_vendor_items_collection';
    protected $_eventObject = 'vendor_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Elogic\Vendor\Model\Vendor', 'Elogic\Vendor\Model\ResourceModel\Vendor');
    }

}

